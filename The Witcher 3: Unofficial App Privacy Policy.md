# The Witcher 3: Unofficial App (Privacy Policy)

This privacy policy governs your use of the software application The Witcher 3: Unofficial App (“the app”) for iOS and Android devices that was created by Loris Stavrinides, Jon Fulton and Tim Hughes ("the creators"). The Application is a non-commerical fan app for The Witcher 3: Wild Hunt and has been created independently from CD PROJEKT RED. CD PROJEKT®, The Witcher® are registered trademarks of CD PROJEKT Capital Group. The Witcher game © CD PROJEKT S.A. Developed by CD PROJEKT S.A. All rights reserved. The Witcher game is set in the universe created by Andrzej Sapkowski in his series of books. All other copyrights and trademarks are the property of their respective owners.”

By using this application, you are agreeing to be bound by these terms and conditions, and are responsible for compliance with any applicable local laws. If you do not agree with these terms and conditions then close this application.


#### Disclaimer of Warranties and Limitation of Liability
The app uses intellectual property from CD PROJEKT RED, rights to use their content within the app has been granted providing certain conditions are met.


#### Automatically Collected Information
The app may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile devices advertising ID, the IP address of your mobile device, your mobile operating system, and information about the way you use the Application.


#### Does the app collect precise real time location information of the device?
The app does not collect precise information about the location of your mobile device.


#### Changes
This Privacy Policy may be updated from time to time for any reason. We will notify you of any changes to our Privacy Policy by posting the new Privacy Policy here. You are advised to consult this Privacy Policy regularly for any changes, as continued use is deemed approval of all changes.


#### Your Consent
By using the app, you are consenting to our processing of your information as set forth in this Privacy Policy now and as amended by us.


#### Contact us
If you have any questions regarding privacy while using the Application, or have questions about our practices, please contact us via email at hello@ratedapp.co.uk.